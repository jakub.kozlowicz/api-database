"""Validation schemas for credits endpoint"""

from marshmallow import Schema, fields
from marshmallow.validate import Length


class CreditsGetInputSchema(Schema):
    """Validation schema for GET method"""

    credit_id = fields.Int(default=None)
    user_id = fields.Int(default=None)
    currency = fields.Str(default=None)


class CreditsPostInputSchema(Schema):
    """Validation schema for POST method"""

    user_id = fields.Int(required=True)
    value = fields.Int(required=True)
    currency = fields.Str(required=True, validate=Length(max=3))
    bank_name = fields.Str(required=True, validate=Length(max=255))


class CreditsPutInputSchema(Schema):
    """Validation schema for PUT method"""

    credit_id = fields.Int(required=True)
    value = fields.Int(required=True)


class CreditsPatchInputSchema(Schema):
    """Validation schema for PATCH method"""

    credit_id = fields.Int(required=True)
    currency = fields.Str(required=True)


class CreditsDeleteInputSchema(Schema):
    """Validation schema for DELETE method"""

    credit_id = fields.Int(required=True)
