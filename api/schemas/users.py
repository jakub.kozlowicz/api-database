"""Validation schemas for users endpoint"""

from marshmallow import Schema, fields
from marshmallow.validate import Length


class UsersGetInputSchema(Schema):
    """Validation schema for GET method"""

    user_id = fields.Int(default=None)
    full_name = fields.Str(default=None, validators=Length(max=255))


class UsersPostInputSchema(Schema):
    """Validation schema for POST method"""

    full_name = fields.Str(required=True, validators=Length(max=255))


class UsersPutInputSchema(Schema):
    """Validation schema for PUT method"""

    user_id = fields.Int(required=True)
    full_name = fields.Str(required=True, validators=Length(max=255))


class UsersDeleteInputSchema(Schema):
    """Validation schema for DELETE method"""

    user_id = fields.Int(required=True)
