"""Wrapper for connecting to MySQL Server on GCP"""

import json
from pathlib import Path

import pymysql
import sqlalchemy
from google.cloud.sql.connector import Connector

CONFIG_FILE = Path(__file__).parent / "config.json"


class Connection:
    """Wrapper class for connecting to MySQL Server on Google Cloud Platform

    Attributes:
        config_file: Configuration files containing credentials for connecting to database
        pool: Connection pool
    """

    def __init__(self, config_file: Path = CONFIG_FILE) -> None:
        self.config_file = config_file
        self.pool = sqlalchemy.create_engine(
            "mysql+pymysql://",
            creator=self.__get_connection,
        )

    def __get_config(self) -> dict[str, str]:
        with open(self.config_file, "r", encoding="utf-8") as file:
            config: dict = json.load(file)

        return config

    def __get_connection(self) -> pymysql.connections.Connection:
        config = self.__get_config()

        with Connector() as connector:
            connection: pymysql.connections.Connection = connector.connect(
                config["connection_string"], config["driver"], **config["credentials"]
            )
        return connection

    def connect(self) -> sqlalchemy.engine.Connection:
        """Connect to database

        Returns:
            Connection pool
        """
        return self.pool.connect()

    def select(
        self, fields: str, table: str, where: str = "1=1"
    ) -> list[sqlalchemy.engine.row.Row]:
        """Execute SELECT query on database

        Args:
            fields (str): Fields to select
            table (str): Table from which select data
            where (str): Specification which data to select

        Returns:
            list[Row]: List containing rows from database
        """
        query = sqlalchemy.text(f"SELECT {fields} FROM {table} WHERE {where}")
        with self.connect() as db_conn:
            result = db_conn.execute(query).fetchall()

        return result

    def insert(
        self, table: str, columns: str, values: str
    ) -> sqlalchemy.engine.cursor.CursorResult:
        """Execute INSERT query on database

        Args:
            table (str): Table to which insert data
            columns (str): Columns in which data will be inserted
                format `(column1, column2, ...)`
            values (str): Values to be inserted in format `('value1', 'value2', ...)`

        Returns:
            CursorResult: Result of query execution
        """
        query = sqlalchemy.text(f"INSERT INTO {table} {columns} VALUES {values}")
        with self.connect() as db_conn:
            result = db_conn.execute(query)

        return result

    def update(
        self, table: str, values: str, where: str
    ) -> sqlalchemy.engine.cursor.CursorResult:
        """Execute UPDATE query on database

        Args:
            table (str): Table in which update data
            values (str): Updated values in format `column1 = value1, column2 = value2, ...`
            where (str): Row specification condition

        Returns:
            CursorResult: Result of query execution
        """
        query = sqlalchemy.text(f"UPDATE {table} SET {values} WHERE {where}")
        with self.connect() as db_conn:
            result = db_conn.execute(query)

        return result

    def delete(self, table: str, where: str) -> sqlalchemy.engine.cursor.CursorResult:
        """Execute DELETE query on database

        Args:
            table (str): Table from which delete data
            where (str): Data specification

        Returns:
            CursorResult: Result of query execution
        """
        query = sqlalchemy.text(f"DELETE FROM {table} WHERE {where}")
        with self.connect() as db_conn:
            result = db_conn.execute(query)

        return result
