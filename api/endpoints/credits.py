"""Functionality of credits endpoint"""

import requests
from flask import request
from flask_restful import Resource

from api.database_connection.google import Connection
from api.endpoints.utils import construct_respone
from api.schemas.credits import (
    CreditsDeleteInputSchema,
    CreditsGetInputSchema,
    CreditsPatchInputSchema,
    CreditsPostInputSchema,
    CreditsPutInputSchema,
)


class Credits(Resource):
    """Credits endpoint functionality

    Attributes:
        db_con: Connection to database handle
    """

    def __init__(self):
        self.db_con = Connection()

    def get(self):
        """Get method for credits endpoint

        Returns:
            JSON data from database
        """
        params = request.args
        schema = CreditsGetInputSchema()
        errors = schema.validate(params)

        if errors:
            data = {"message": "Parameters validation error"}
            return construct_respone(400, data)

        credit_id = params.get("credit_id", default=None)
        user_id = params.get("user_id", default=None)
        currency = params.get("currency", default=None)

        data = {}

        if not any([credit_id, user_id, currency]):
            credits_data_db = self.db_con.select("*", "CREDITS")

            credits_data = []
            for idx, user_id, credit_value, currency, bank_name in credits_data_db:
                credits_data.append(
                    {
                        "id": idx,
                        "userId": user_id,
                        "creditValue": credit_value,
                        "currency": currency,
                        "bankName": bank_name,
                    }
                )

            data = {"credits": credits_data}

        elif all([credit_id, currency]) and user_id is None:
            credits_data_db = self.db_con.select(
                "*", "CREDITS", where=f"ID = {credit_id}"
            )

            value_db = credits_data_db[0][2]
            currency_db = credits_data_db[0][3]

            if currency_db == currency:
                credits_data = []
                idx, user_id, credit_value, currency, bank_name = credits_data_db[0]
                credits_data.append(
                    {
                        "id": idx,
                        "userId": user_id,
                        "creditValue": credit_value,
                        "currency": currency,
                        "bankName": bank_name,
                    }
                )

                data = {"credits": credits_data}
            else:
                url = (
                    "https://api.exchangerate.host/convert?"
                    f"from={currency_db}&to={currency}&amount={value_db}"
                )
                response = requests.get(url)
                result = int(response.json()["result"])

                credits_data = []
                idx, user_id, _, _, bank_name = credits_data_db[0]
                credits_data.append(
                    {
                        "id": idx,
                        "userId": user_id,
                        "creditValue": result,
                        "currency": currency,
                        "bankName": bank_name,
                    }
                )

                data = {"credits": credits_data}

        elif credit_id is not None and not any([user_id, currency]):
            credits_data_db = self.db_con.select(
                "*", "CREDITS", where=f"ID = {credit_id}"
            )

            credits_data = []
            for idx, user_id, credit_value, currency, bank_name in credits_data_db:
                credits_data.append(
                    {
                        "id": idx,
                        "userId": user_id,
                        "creditValue": credit_value,
                        "currency": currency,
                        "bankName": bank_name,
                    }
                )

            data = {"credits": credits_data}

        elif user_id is not None and not any([credit_id, currency]):
            credits_data_db = self.db_con.select(
                "*", "CREDITS", where=f"USER_ID = {user_id}"
            )

            credits_data = []
            for idx, user_id, credit_value, currency, bank_name in credits_data_db:
                credits_data.append(
                    {
                        "id": idx,
                        "userId": user_id,
                        "creditValue": credit_value,
                        "currency": currency,
                        "bankName": bank_name,
                    }
                )

            data = {"credits": credits_data}

        return construct_respone(200, data)

    def post(self):
        """POST method for credits endpoint

        Returns:
            JSON data of content which were added to database
        """
        params = request.args
        schema = CreditsPostInputSchema()
        errors = schema.validate(params)

        if errors:
            data = {"message": "Parameters validation error"}
            return construct_respone(400, data)

        user_id = params.get("user_id")
        credit_value = params.get("value")
        currency = params.get("currency")
        bank_name = params.get("bank_name")

        self.db_con.insert(
            "CREDITS",
            "(USER_ID, CREDIT_VALUE, CURRENCY, BANK_NAME)",
            f"({user_id}, {credit_value}, '{currency}', '{bank_name}')",
        )

        data = {
            "message": "Added credit to database",
        }

        return construct_respone(200, data)

    def put(self):
        """PUT method for credits endpoint

        Returns:
            JSON data of content which changed in database
        """
        params = request.args
        schema = CreditsPutInputSchema()
        errors = schema.validate(params)

        if errors:
            data = {"message": "Parameters validation error"}
            return construct_respone(400, data)

        credit_id = params.get("credit_id")
        credit_value = params.get("value")

        self.db_con.update(
            "CREDITS", f"CREDIT_VALUE = {credit_value}", f"ID = {credit_id}"
        )

        data = {
            "message": f"Changed value of the credit {credit_id}",
        }

        return construct_respone(200, data)

    def patch(self):
        """PATCH method for credits endpoint

        Returns:
            JSON data of content which changed in database
        """
        params = request.args
        schema = CreditsPatchInputSchema()
        errors = schema.validate(params)

        if errors:
            data = {"message": "Parameters validation error"}
            return construct_respone(400, data)

        credit_id = params.get("credit_id")
        currency = params.get("currency")

        credits_data_db = self.db_con.select(
            "CREDIT_VALUE, CURRENCY", "CREDITS", where=f"ID={credit_id}"
        )

        value_db = credits_data_db[0][0]
        currency_db = credits_data_db[0][1]

        if currency_db != currency:
            url = (
                "https://api.exchangerate.host/convert?"
                f"from={currency_db}&to={currency}&amount={value_db}"
            )
            response = requests.get(url)
            result = int(response.json()["result"])

            self.db_con.update(
                "CREDITS",
                f"CREDIT_VALUE = {result}, CURRENCY = '{currency}'",
                where=f"ID={credit_id}",
            )

            data = {
                "message": f"Changed credit currency and value from {currency_db} to {currency}"
            }
        else:
            data = {"message": "Currency is identical, so do not change"}

        return construct_respone(200, data)

    def delete(self):
        """DELETE method for credits endpoint

        Returns:
            JSON data which of contents which were deleted from database
        """
        params = request.args
        schema = CreditsDeleteInputSchema()
        errors = schema.validate(params)

        if errors:
            data = {"message": "Parameters validation error"}
            return construct_respone(400, data)

        credit_id = params.get("credit_id")

        self.db_con.delete("CREDITS", f"ID={credit_id}")

        data = {
            "message": f"Deleted credit {credit_id} from database",
        }

        return construct_respone(200, data)
