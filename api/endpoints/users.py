"""Functionality of users endpoint"""

from flask import request
from flask_restful import Resource

from api.database_connection.google import Connection
from api.endpoints.utils import construct_respone
from api.schemas.users import (
    UsersDeleteInputSchema,
    UsersGetInputSchema,
    UsersPostInputSchema,
    UsersPutInputSchema,
)


class Users(Resource):
    """Users endpoint functionality

    Attributes:
        db_con: Connection to database handle
    """

    def __init__(self):
        self.db_con = Connection()

    def get(self):
        """Get method for users endpoint

        Returns:
            JSON data from database
        """
        params = request.args
        schema = UsersGetInputSchema()
        errors = schema.validate(params)

        if errors:
            data = {"message": "Parameters validation error"}
            return construct_respone(400, data)

        user_id = params.get("user_id", default=None)
        full_name = params.get("full_name", default=None)

        data = {}

        if not any([full_name, user_id]):
            users_data_db = self.db_con.select(fields="*", table="USERS")

            users_data = []
            for idx, name in users_data_db:
                users_data.append({"userId": idx, "fullName": name})

            data = {
                "users": users_data,
            }
        elif all([full_name, user_id]):
            users_data_db = self.db_con.select(
                "*", "USERS", where=f"ID = {user_id} AND FULL_NAME = '{full_name}'"
            )

            users_data = []
            for idx, name in users_data_db:
                users_data.append({"userId": idx, "fullName": name})

            data = {
                "users": users_data,
            }
        elif user_id is not None:
            users_data_db = self.db_con.select("*", "USERS", where=f"ID = {user_id}")

            users_data = []
            for idx, name in users_data_db:
                users_data.append({"userId": idx, "fullName": name})

            data = {
                "users": users_data,
            }
        elif full_name is not None:
            users_data_db = self.db_con.select(
                "*", "USERS", where=f"FULL_NAME = '{full_name}'"
            )

            users_data = []
            for idx, name in users_data_db:
                users_data.append({"userId": idx, "fullName": name})

            data = {
                "users": users_data,
            }

        return construct_respone(200, data)

    def post(self):
        """POST method for users endpoint

        Returns:
            JSON data of content which were added to database
        """
        params = request.args
        schema = UsersPostInputSchema()
        errors = schema.validate(params)

        if errors:
            data = {"message": "Parameters validation error"}
            return construct_respone(400, data)

        full_name = params.get("full_name")

        self.db_con.insert("USERS", "(FULL_NAME)", f"('{full_name}')")

        data = {
            "message": f"Added user with name {full_name} to database",
        }

        return construct_respone(200, data)

    def put(self):
        """PUT method for users endpoint

        Returns:
            JSON data of content which changed in database
        """
        params = request.args
        schema = UsersPutInputSchema()
        errors = schema.validate(params)

        if errors:
            data = {"message": "Parameters validation error"}
            return construct_respone(400, data)

        user_id = params.get("user_id")
        full_name = params.get("full_name")

        self.db_con.update("USERS", f"FULL_NAME = '{full_name}'", f"ID = {user_id}")

        data = {
            "message": f"Changed name of the user {user_id} to {full_name}",
        }

        return construct_respone(200, data)

    def delete(self):
        """DELETE method for users endpoint

        Returns:
            JSON data which of contents which were deleted from database
        """
        params = request.args
        schema = UsersDeleteInputSchema()
        errors = schema.validate(params)

        if errors:
            data = {"message": "Parameters validation error"}
            return construct_respone(400, data)

        user_id = params.get("user_id")

        self.db_con.delete("USERS", f"ID={user_id}")

        data = {
            "message": f"Deleted user with ID {user_id} from database",
        }

        return construct_respone(200, data)
