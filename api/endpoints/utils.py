"""Set of utils used in endpoint logic"""

from flask import jsonify, make_response
from flask.wrappers import Response


def construct_respone(code: int, body: dict) -> Response:
    """Construct request response

    Args:
        code (int): Starus code
        body (dict): Response body

    Returns:
        Response: Request response
    """
    response = make_response(jsonify(body), code)
    response.headers["Content-Type"] = "application/json"
    return response
