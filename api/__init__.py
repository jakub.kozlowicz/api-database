"""Main application entrypoint"""

from flask import Flask
from flask_restful import Api

from api.endpoints.credits import Credits
from api.endpoints.users import Users

app = Flask(__name__)
api = Api(app)

api.add_resource(Users, "/users")
api.add_resource(Credits, "/credits")

if __name__ == "__main__":
    app.run()
