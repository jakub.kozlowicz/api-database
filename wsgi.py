#!/usr/bin/env python3

import sys
import logging
import os

os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "/path/to/db.json"

logging.basicConfig(stream=sys.stderr)
sys.path.insert(0, os.getcwd() + "/api")

from api import app as application

application.secret_key = "secret"
