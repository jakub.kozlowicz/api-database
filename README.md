# api-database

REST-API written in Python which connect to MySQL database on Google Cloud Platform.  
Project created for university course called "Databases" at Wroclaw University of Science and Technology.

## Configuration

This API is connected to MySQL server on Google Cloud Platform. In order to protect personal data under `api/database_connection/` there is only example configuration. To properly run application make your own config file named `config.json` in this directory. You can also copy `example_config.json` and start from there.

## Usage

Before you start make sure you have generated authentication key for Google API. *For more information, please see https://cloud.google.com/docs/authentication/getting-started*

After generating and downloading key make sure that the environment variable is set. For example
```
export GOOGLE_APPLICATION_CREDENTIALS="/home/jakub/db.json"
```

1. Create virtual environment
```
python3 -m venv .venv
source .venv/bin/activate
```

2. Install all required modules
```
pip install -r requirements.txt
```

3. Run app
```
flask run
```

## License

MIT License
Copyright (c) 2022, Jakub Kozłowicz
